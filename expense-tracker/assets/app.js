var state = {
	expenses: [
		{
			item: 'Learn ES book',
			price: 120
		}
	]
};

document.getElementById('expenseForm')
	.addEventListener('submit', function(event){
		event.preventDefault();
		var item, price;
		item = document.getElementById('item').value;
		price = document.getElementById('price').value;
		state.expenses.push({
			item: item,
			price: price
		});
		updateExpenseTable();
	}, false);

function updateExpenseTable(){
	var tableBody = document.getElementById('expenseTableBody');
	tableBody.innerHTML = '';
	state.expenses.forEach(function(expense, index){
		tableBody.innerHTML +=
			'<tr>' +
				'<th scope="row">' + index + '</th>' +
				'<td>' + expense.item + '</td>' +
				'<td>' + expense.price + '</td>' +
			'</tr>';
	});
}

updateExpenseTable();