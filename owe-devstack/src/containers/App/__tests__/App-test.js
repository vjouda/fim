import React from 'react';
import {shallow} from 'enzyme';
import App from '../App';

describe('App should', () => {
	it('render without errors', () => {
		const app = shallow(<App/>);
		expect(app).toBePresent();
	});
});