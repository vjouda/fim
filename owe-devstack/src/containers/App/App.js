import React from "react";
import reactLogo from "../../images/react-logo.png";

export default () => {
    return (
        <div>
            <h1>Hello React!!</h1>
            <img src={reactLogo} width={250} height={250}/>
        </div>
    )
}