import path from "path";

export default {
	output: {
		path: path.resolve(__dirname, './build/public'),
	},
	port: 3330
};
