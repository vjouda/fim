import webpack from "webpack";
import path from "path";

export default {
	context: path.resolve(__dirname, './src'),
	entry: [
		'./index'
	],
	plugins: [
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				screw_ie8: true,
				warnings: true,
			},
			mangle: {
				screw_ie8: true,
			},
			output: {
				comments: false,
				screw_ie8: true,
			},
		}),
		new webpack.optimize.AggressiveMergingPlugin()
	],
	output: {
		path: path.resolve(__dirname, './build/public/assets'),
		publicPath: '/assets',
		sourcePrefix: '  ',
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loaders: ['babel-loader'],
			}, {
				test: /\.scss$/,
				loaders: ['style', 'css', 'resolve-url?sourceMap', 'sass?sourceMap'],
			}, {
				test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/,
				loaders: ['url-loader'],
			}, {
				test: /\.(eot|ttf|wav|mp3)$/,
				loaders: ['file-loader'],
			}
		]
	},
	stats: {
		colors: true,
		errorDetails: true
	},
};
