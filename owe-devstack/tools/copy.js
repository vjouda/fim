import ncp from 'ncp';
import promisify from 'es6-promisify';

const asyncNcp = promisify(ncp);

function copy() {
	return Promise.all([
		asyncNcp('src/public', 'build/public'),
	]);
}

export default copy;
