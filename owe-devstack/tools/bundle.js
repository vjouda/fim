import webpack from "webpack";
import webpackConfig from "../webpack-prod.config";

/**
 * Creates application bundles from the source files.
 */
function bundle() {
	return new Promise((resolve, reject) => {
		webpack(webpackConfig).run((err, stats) => {
			console.log(stats.toString(webpackConfig.stats));
			if (err) {
				return reject(err);
			}
			return resolve();
		});
	});
}

export default bundle;
