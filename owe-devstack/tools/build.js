import run from './run';
import clean from './clean';
import copy from './copy';
import bundle from './bundle';

function build(){
	return Promise.resolve()
		.then(() => run(clean))
		.then(() => run(copy))
		.then(() => run(bundle));
}

export default build;