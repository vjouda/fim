import del from "del";
import mkdirp from "mkdirp-promise";

function clean() {
	return Promise.resolve()
		.then(() => del(['build/*']))
		.then(() => mkdirp('build/public'));
}

export default clean;
