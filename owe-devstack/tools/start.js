import run from "./run";
import clean from "./clean";
import copy from "./copy";
import startDevServer from "./startDevServer";

function start() {
	return run(clean)
		.than(() => run(copy))
		.than(() => run(startDevServer));
}

export default start;
