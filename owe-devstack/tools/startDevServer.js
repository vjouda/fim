import browserSync from "browser-sync";
import historyApiFallback from "connect-history-api-fallback";
import webpack from "webpack";
import webpackDevMiddleware from "webpack-dev-middleware";
import webpackHotMiddleware from "webpack-hot-middleware";
import webpackConfig from "../webpack-dev.config";
import serverConfig from "../server-dev.config";

async function startServer() {
	return new Promise((resolve) => {
		const compiler = webpack(webpackConfig);
		const handleCompilerComplete = () => {
			const bs = browserSync.create();
			bs.init({
				ui: false,
				port: serverConfig.port,
				server: {
					baseDir: serverConfig.output.path,
					middleware: [
						historyApiFallback({
							disableDotRule: true,
							verbose: true,
							rewrites: [
								{
									from: new RegExp('/assets'),
									to: (context) => `${context.parsedUrl.pathname}`,
								}
							]
						}),
						webpackDevMiddleware(compiler, {
							publicPath: webpackConfig.output.publicPath,
							stats: webpackConfig.stats,
						}),
						webpackHotMiddleware(compiler)
					],
				},
				files: [
					'**/*.css',
				]
			}, resolve);
		};
		compiler.run(handleCompilerComplete);
	});
}

export default startServer;
