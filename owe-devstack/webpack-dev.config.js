import webpack from "webpack";
import path from "path";

export default {
	context: path.resolve(__dirname, './src'),
	entry: [
		'react-hot-loader/patch',
		'webpack-hot-middleware/client',
		'./index'
	],
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin()
	],
	output: {
		path: path.resolve(__dirname, './build/public/assets'),
		publicPath: '/assets',
		sourcePrefix: '  ',
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loaders: ['babel-loader'],
			}, {
				test: /\.scss$/,
				loaders: ['style', 'css', 'resolve-url?sourceMap', 'sass?sourceMap'],
			}, {
				test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/,
				loaders: ['url-loader'],
			}, {
				test: /\.(eot|ttf|wav|mp3)$/,
				loaders: ['file-loader'],
			}
		]
	},
	devtool: 'source-map',
	stats: {
		colors: true,
		errorDetails: true
	},
};
